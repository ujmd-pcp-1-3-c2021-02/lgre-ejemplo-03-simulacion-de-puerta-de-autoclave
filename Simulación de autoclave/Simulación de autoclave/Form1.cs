﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulación_de_autoclave
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = true;
            tmrCerrar.Enabled = false;

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            tmrCerrar.Enabled = true;
            tmrAbrir.Enabled = false;
        }

        private void tmrAbrir_Tick(object sender, EventArgs e)
        {
            if (PanelPuertaFrnt.Height >=5)
            {
                PanelPuertaFrnt.Height = PanelPuertaFrnt.Height - 5;
                PanelPuertaFrnt.Top = PanelPuertaFrnt.Top + 5;
                }
                else
                {
                    tmrAbrir.Enabled = false;
                }
        }

        private void tmrCerrar_Tick(object sender, EventArgs e)
        {
            if (PanelPuertaFrnt.Height <= 160)
            {
                PanelPuertaFrnt.Height = PanelPuertaFrnt.Height + 5;
                PanelPuertaFrnt.Top = PanelPuertaFrnt.Top - 5;
            }
            else
            {
                tmrCerrar.Enabled = false;
            }
}
    }
}
